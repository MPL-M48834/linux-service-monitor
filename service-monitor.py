#!/usr/bin/python
#Service-monitor.py is a simple script which takes a command line input. 
#Monitors active services for a string that matches the command line input (excluding the grep and python commands),
#IF the service is active, continue to monitor, IF NOT push a notification to teams service notification channel via webhook
import commands
import time
import socket
import json
import sys
import requests
while True:
    #Query PROCESS
    PROCESS = sys.argv[1]
    QUERY = commands.getoutput('systemctl is-active %s' % PROCESS).strip()
    HOST    = socket.getfqdn()    
    
    if(QUERY == "active"):
        #PROCESS is running therefore next
        next
    else:
        #PROCES is not running therefore post to Teams webhook
        WEBHOOK="https://outlook.office.com/webhook/7d89dae9-6376-4af5-a087-7b0b94abe57c@b89d3e4d-38fd-4768-9d71-0b561b89f428/IncomingWebhook/7bd6699e8fb74275818615bcca4bcc44/77f85261-a15b-4666-9659-8374f24c41e5"
        CONTENT={'text':"Process %s has stopped running on %s" 
        %(PROCESS,HOST)
        } 
 
        #Craft response
        RESPONSE = requests.post(
            WEBHOOK, data=json.dumps(CONTENT),
            headers={'Content-Type': 'application/json'}
        )
        #If response != 200, whoops something went wrong
        if RESPONSE.status_code != 200:
            raise ValueError(
                'Request to Teams returned an error %s, the response is:\n%s'
                % (RESPONSE.status_code, RESPONSE.text)
            )
    #Check every 1 minute
    time.sleep(1 * 60)